'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SSICompileWebpackPlugin = require('ssi-compile-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.join(__dirname, 'dist/dflip/scripts'),
		filename: 'main.bundle.js',
		chunkFilename: '[name].bundle.js'
	},
	devServer: {
		static: {
			directory: path.join(__dirname, 'dist')
		},
		port: 8080,
		hot: false,
		inline: false
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin(),
		new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
			_: 'underscore'
        })
		// new webpack.HotModuleReplacementPlugin()
		// new HtmlWebpackPlugin({

		// }),
		// new SSICompileWebpackPlugin({
		// 	publicPath: '',
        //     localBaseDir: './dist',
        //     minify: false
		// })
	],
	module: {
		rules: [
			{
				test: require.resolve('jquery'),
				use: [
					{
						loader: 'expose-loader',
						options: {
							exposes: ["$", "jQuery"]
						}
					}
				]
			},
			{
				test: /\.css$/i,
				use: [
					{
						loader: 'style-loader',
						options: {
							injectType: 'singletonStyleTag'
						}
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [
									[
										'autoprefixer'
									]
								]
							}
						}
					}
				]
			},
			{
				test: /\.scss$/i,
				use: [
					{
						loader: 'style-loader',
						options: {
							injectType: 'singletonStyleTag'
						}
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								outputStyle: 'compressed', // Node Sass 的可傳遞選項
							}
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [
									[
										'autoprefixer'
									]
								]
							}
						}
					}
				]
			},
			{
				test: /\.(jpg|jpeg|png|gif|svg)$/i,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 10240
						}
					}
				]
			},
			{
				test: /\.(woff|woff2|ttf|otf|eot)$/i,
				use: [
					{
						loader: 'file-loader'
					}
				]
			},
			{
				test: /\.js$/i,
				exclude: /(node_modules|bower_components)/,
				use: 'babel-loader'
			},
			{
				test: /\.glsl$/i,
				use: 'raw-loader'
			},
		]
	},
	watchOptions: {
		ignored: /node_modules/,
		aggregateTimeout: 300,
		poll: 1000
	},
	performance: {
        hints: 'error',
        maxAssetSize: 100000000,
        maxEntrypointSize: 10000000
    }
}