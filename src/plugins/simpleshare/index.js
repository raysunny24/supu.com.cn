import closeIMg from './close.png';
import './index.scss';

export default function SimpleShare(options) {
    // get share content
    options = options || {};
    var url = options.url || window.location.href;
    var title = options.title || document.title;
    var content = options.content || '';
    var pic = options.pic || '';

    // fix content format
    url = encodeURIComponent(url);
    title = encodeURIComponent(title);
    content = encodeURIComponent(content);
    pic = encodeURIComponent(pic);

    // share target url
    var qzone = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url={url}&title={title}&pics={pic}&summary={content}';
    var weibo = 'http://service.weibo.com/share/share.php?url={url}&title={title}&pic={pic}&searchPic=false';
    var tqq = 'http://share.v.t.qq.com/index.php?c=share&a=index&url={url}&title={title}&appkey=801cf76d3cfc44ada52ec13114e84a96';
    var renren = 'http://widget.renren.com/dialog/share?resourceUrl={url}&srcUrl={url}&title={title}&description={content}';
    var douban = 'http://www.douban.com/share/service?href={url}&name={title}&text={content}&image={pic}';
    var facebook = 'https://www.facebook.com/sharer/sharer.php?u={url}&t={title}&pic={pic}';
    var twitter = 'https://twitter.com/intent/tweet?text={title}&url={url}';
    var linkedin = 'https://www.linkedin.com/shareArticle?title={title}&summary={content}&mini=true&url={url}&ro=true';
    var weixin = 'https://api.qrserver.com/v1/create-qr-code/?size=120x120&data={url}';
    var qq = 'http://connect.qq.com/widget/shareqq/index.html?url={url}&desc={title}&pics={pic}';
    var tumblr = 'https://www.tumblr.com/widgets/share/tool?posttype=link&canonicalUrl={url}&title={title}&content={content}';
    var pinterest = 'https://www.pinterest.com/pin/create/button/?url={url}&media=" + encodeURIComponent(a))';

    // replace content functions
    function replaceAPI (api) {
        api = api.replace('{url}', url);
        api = api.replace('{title}', title);
        api = api.replace('{content}', content);
        api = api.replace('{pic}', pic);

        return api;
    }

    // share target
    this.qzone = function() {
        window.open(replaceAPI(qzone));
    };
    this.weibo = function() {
        window.open(replaceAPI(weibo));
    };
    this.tqq = function() {
        window.open(replaceAPI(tqq));
    };
    this.renren = function() {
        window.open(replaceAPI(renren));
    };
    this.douban = function() {
        window.open(replaceAPI(douban));
    };
    this.facebook = function() {
        window.open(replaceAPI(facebook));
    };
    this.twitter = function() {
        window.open(replaceAPI(twitter));
    };
    this.linkedin = function() {
        window.open(replaceAPI(linkedin));
    };
    this.qq = function() {
        window.open(replaceAPI(qq));
    };
    this.tumblr = function() {
        window.open(replaceAPI(tumblr));
    };
    this.pinterest = function() {
        window.open(replaceAPI(pinterest));
    };
    this.weixin = function(callback) {
        if (!callback) {
            // window.open(replaceAPI(weixin));
            // var wxHtml = '<div class="wx-share"><i class="wx-share-close"></i><img src="'+replaceAPI(weixin)+'" alt=""><p>分享到微信朋友圈</p></div>';
            
            let wxShareBox = document.createElement('div');
            wxShareBox.className = 'wx-share';
            let wxCloseBtn = document.createElement('i');
            wxCloseBtn.className = 'wx-share-close';
            let wxCloseImg = new Image();
            wxCloseImg.src = closeIMg;
            wxCloseBtn.appendChild(wxCloseImg);
            let wxShareTitle = document.createElement('p');
            wxShareTitle.innerText = '分享到微信朋友圈';
            let wxQrcode = new Image();
            wxQrcode.src = replaceAPI(weixin);


            wxQrcode.onload = function(){
                wxShareBox.appendChild(wxCloseBtn);
                wxShareBox.appendChild(wxQrcode);
                wxShareBox.appendChild(wxShareTitle);
                document.body.appendChild(wxShareBox);
                wxCloseBtn.addEventListener('click', function(){
                    wxShareBox.parentNode.removeChild(wxShareBox);
                })
            }
            
        }else{
            callback(replaceAPI(weixin));
        }
    };


    this.goShare = function(type) {
        switch (type){
            case 'twitter':
                this.twitter();
                break;
            case 'facebook':
                this.facebook();
                break;
            case 'linkedin':
                this.linkedin();
                break;
            case "weixin":
                this.weixin();
                break;
            case "weibo":
                this.weibo();
                break;
            case 'tumblr':
                this.tumblr();
                break;
            case 'pinterest':
                this.pinterest();
                break;
            default:
                break;
        }
    }
}