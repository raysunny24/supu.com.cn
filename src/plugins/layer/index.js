import './layer.scss';
import Language from '../../language';
const lang = new Language();

const layer = {
	msg: (title, opt, fn) => {
		const layerHtml = `<div class="plugin-layer-box msg"><p>${title}</p></div>`;
		$('body').append(layerHtml);

		if (opt) {
			setTimeout(() => {
				fn();
				$('.plugin-layer-box').remove();
			}, opt.time)
		} else {
			setTimeout(() => {
				$('.plugin-layer-box').remove();
			}, 2000)
		}
	},
	dialog: (title, btns, fun) => {
		const layerBox = document.createElement('div');
		layerBox.classList = 'plugin-layer-box dialog';
		const layerMask = document.createElement('div');
		layerMask.className= "plugin-layer-mask";
		layerMask.addEventListener('click', function(){
			$('.plugin-layer-box').remove();
		}, false);
		const layerMain = document.createElement('div');
		layerMain.className = "plugin-layer-main";
		const layerContent = document.createElement('div');
		layerContent.className = "plugin-layer-content";
		layerContent.innerHTML = title;
		const layerBtns = document.createElement('div');
		layerBtns.className = "plugin-layer-btns";
		for (let i = 0; i < btns.length; i++) {
			const btnItem = document.createElement('span');
			btnItem.className = 'plugin-layer-btn';
			btnItem.innerHTML = btns[i];
			layerBtns.appendChild(btnItem);
			btnItem.addEventListener('click', function(){
				$('.plugin-layer-box').remove();
				fun[i]();
			}, false);
		}
		layerBox.appendChild(layerMask);
		layerMask.appendChild(layerMain);
		layerMain.appendChild(layerContent);
		layerMain.appendChild(layerBtns);
		$('body').append(layerBox);
	},
	
	chat: (func) => {
		const layerHtml = document.createElement('div');
		const html = `<div class="clayer">
						<div class="clayer-bg"></div>
						<div class="clayer-main">
							<div class="main">
								<div class="wrap">
									<div class="clayer-body bg-white">
										<div class="clayerb-wrap">
											<div class="clayer-img fz-100 img-box">
												<img src="/themes/cn/default/assets/images/layer_promise.png" alt="">
											</div>
											<h3 class="fz-28 fw-700 black text-center mt-40">`+lang.layerChatTitle+`</h3>
											<div class="clayer-desc fz-24 fw-300 black mt-40 text-center">
											`+lang.layerChatDesc+`
											</div>

											<button type="button" class="btn-default btn-confirm fz-28 bg-blue white mt-60">`+lang.layerChatBtn+`</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>`;

		layerHtml.innerHTML = html;

		const btnConfirm = layerHtml.querySelector('.btn-confirm');

		btnConfirm.addEventListener('click', function(){
			func();
			layerHtml.remove();
		});

		document.body.appendChild(layerHtml);
		// $('.dta-layer').fadeIn('fast');
	},

	phoneSuccess: (func) => {
		const layerHtml = document.createElement('div');
		const html = `<div class="clayer">
						<div class="clayer-bg"></div>
						<div class="clayer-main">
							<div class="main">
								<div class="wrap">
									<div class="clayer-body bg-white">
										<div class="clayerb-wrap">
											<div class="clayer-img fz-100 img-box">
												<img src="/themes/cn/default/assets/images/layer_success.png" alt="">
											</div>
											<h3 class="fz-24 fw-400 black text-center mt-40">`+lang.layerPhoneSuccessTitle+`</h3>
											<div class="clayer-pwd fz-60 mt-40"><span class="bg-lightblue blue">1</span><span class="bg-lightblue blue">1</span><span class="bg-lightblue blue">1</span><span class="bg-lightblue blue">1</span></div>

											<button type="button" class="btn-default btn-confirm fz-28 bg-blue white mt-60">`+lang.layerPhoneSuccessBtn+`</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>`;

		layerHtml.innerHTML = html;

		const btnConfirm = layerHtml.querySelector('.btn-confirm');

		btnConfirm.addEventListener('click', function(){
			func();
			layerHtml.remove();
		});

		document.body.appendChild(layerHtml);
		// $('.dta-layer').fadeIn('fast');
	},

	phoneErr: (func) => {
		const layerHtml = document.createElement('div');
		const html = `<div class="clayer">
						<div class="clayer-bg"></div>
						<div class="clayer-main">
							<div class="main">
								<div class="wrap">
									<div class="clayer-body bg-white">
										<div class="clayerb-wrap">
											<div class="clayer-img fz-100 img-box">
												<img src="/themes/cn/default/assets/images/layer_error.png" alt="">
											</div>
											<div class="clayer-desc fz-24 fw-300 black mt-40 text-center">
											`+lang.layerPhoneErrTitle+`
											</div>
											<button type="button" class="btn-default btn-confirm fz-28 bg-blue white mt-60">`+lang.layerPhoneErrBtn+`</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>`;

		layerHtml.innerHTML = html;

		const btnConfirm = layerHtml.querySelector('.btn-confirm');

		btnConfirm.addEventListener('click', function(){
			func();
			layerHtml.remove();
		});

		document.body.appendChild(layerHtml);
		// $('.dta-layer').fadeIn('fast');
	},

	loginPolicy: (func) => {
		const layerHtml = document.createElement('div');
		const html = `<div class="clayer">
						<div class="clayer-bg"></div>
						<div class="clayer-main">
							<div class="main">
								<div class="wrap">
									<div class="clayer-body bg-white">
										<div class="clayerb-wrap">
											<div class="clayer-desc fz-22 fw-300 black mt-40">
											`+lang.layerLoginPolicy+`
											</div>
											<button type="button" class="btn-default btn-confirm fz-28 bg-blue white mt-60">`+lang.layerLoginPolicyBtn+`</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>`;

		layerHtml.innerHTML = html;

		const btnConfirm = layerHtml.querySelector('.btn-confirm');

		btnConfirm.addEventListener('click', function(){
			func();
			layerHtml.remove();
		});

		document.body.appendChild(layerHtml);
		// $('.dta-layer').fadeIn('fast');
	},
}

export { layer }