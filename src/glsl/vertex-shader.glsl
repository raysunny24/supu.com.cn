varying vec2 vUv;
varying vec3 vPos;
uniform float uTime;
void main() {
	vUv = uv;
	vPos = position;

	// vPos += cos(uTime * .5);

	gl_Position = projectionMatrix * modelViewMatrix * vec4(vPos, 1.0);

}