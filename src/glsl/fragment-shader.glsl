varying vec2 vUv;
uniform float uTime;
void main() {

	float d = length((vUv - .5) * 2.0);

	float r = .25 / d;

	gl_FragColor = vec4(vec3(.7, .3, .1), r);
	
}
