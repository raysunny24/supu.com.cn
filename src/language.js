let multiLang = {
    cn: {
        required: '此项为必填',
		phoneErr: '请正确填写手机号码',
		emailErr: '邮箱格式不正确',
        more: '更多'
    },
	en: {
        required: 'This field is required',
		phoneErr: 'Please enter valid phone numbers',
		emailErr: 'E-mail format is incorrect',
        more: "More"
    }
}

export default function Language() {
    const langCode = $('#lang').val();

    let lang = null;

    switch (langCode) {
        case 'cn': 
            lang = multiLang.cn;
            break;
        case 'en': 
            lang = multiLang.en;
            break;
    }

    return lang;
}