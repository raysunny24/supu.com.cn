"use strict";

import '@babel/polyfill';

import $, { error } from 'jquery';

// import Lenis from '@studio-freight/lenis';

// import LazyLoad from 'vanilla-lazyload';

// SimpleShare
// import SimpleShare from './plugins/simpleshare/index';

// AOS
// import AOS from 'aos';
// import 'aos/dist/aos.css';

import { layer } from './plugins/layer/index';

// vanilla-cookieconsent
// import cookieConsent from './plugins/cookie-consent';

// import '@splidejs/splide/css';
// import Splide from '@splidejs/splide';
// import { Grid } from '@splidejs/splide-extension-grid';

// import imagesLoaded from 'imagesloaded';
// import jQueryBridget from 'jquery-bridget';
// import Masonry from 'masonry-layout';
// jQueryBridget('masonry', Masonry, $);

import validate from 'jquery-validation';
import axios from 'axios';

// gsap
// import { gsap, Power1, Power3 } from 'gsap';
// import { ScrollTrigger } from "gsap/ScrollTrigger";
// import { MorphSVGPlugin } from "gsap/MorphSVGPlugin";
// gsap.registerPlugin(ScrollTrigger);
// gsap.registerPlugin(MorphSVGPlugin);


// import layer from 'layui-layer';

import Cookies from 'js-cookie';

import './iconfont/iconfont';

import Language from './language';
const lang = new Language();
const langCode = $('#lang').val();

console.log(langCode);


jQuery.validator.addMethod("isMobile", function(value, element) {  
    var length = value.length;  
    var mobile = /^1[3-9]\d{9}$/;  
    return this.optional(element) || (length == 11 && mobile.test(value));  
}, lang.phoneErr);
jQuery.extend(jQuery.validator.messages, {
	required: lang.required,
	email: lang.emailErr
});

(function () {
	$('.pcate-item__header').on('click', function () {
		const i = $(this).parents('.pcate-item').index();

		$('.pcate-item').each(function (index, item) {
			const body = $(item).find('.pcate-sub');
			if (i === index) {
				if ($(item).hasClass('active')) {
					$(item).removeClass('active');
					body.slideUp(300);
				} else {
					$(item).addClass('active');
					body.slideDown(300);
				}
			} else {
				if ($(item).hasClass('active')) {
					$(item).removeClass('active');
					body.slideUp(300);
				}
			}
		});
	});

	if ($('.psearch-input').length) {

		const $input = $('.psearch-input');

		function checkInput() {

			const val = $input.val();
			if (val) {
				$('.psearch').addClass('active');
			} else {
				$('.psearch').removeClass('active');
			}

		}
		$('.psearch-input').on('input', function () {
			checkInput();
		});

		$('.js-psearch-close').on('click', function () {
			$('.psearch').removeClass('active');
			$input.val('');
		});

	}

	if ($('.download-header').length) {
		$(window).on('scroll', function () {
			if ($(window).scrollTop() > 0) {
				$('.download-header').addClass('scroll')
			} else {
				$('.download-header').removeClass('scroll')
			}
		});
	}

})();


(function () {

	function validatePhoneNumber(phoneNumber) {
		return /^1[3-9]\d{9}$/.test(phoneNumber);
	}
	if (document.querySelector('.js-sendCode')) {
		const btn = document.querySelector('.js-sendCode');
		const input = document.querySelector('#tel');
		let second = 60;
		let int;
		btn.addEventListener('click', function () {
			console.log('send Code', input.value);
			if (validatePhoneNumber(input.value)) {
				axios.post('post.shtml', {
					phone: input.value
				}).then(function (res) {
					console.log(res);
					if (res.data == 'ok') {
						console.log('发送成功');
						btn.setAttribute('disabled', true);
						int = setInterval(() => {

							if (second > 0) {
								second--;
								btn.innerHTML = '重新发送（' + second + '）';
							} else {
								btn.setAttribute('disabled', false);
								clearInterval(int);
								btn.innerHTML = '获取验证码';
								second = 60;
							}

						}, 1000);
					}
				})
			} else {
				layer.msg('请输入有效的手机号');
			}
		});
	}

	$('.js-layerm-close').on('click', function(){
		Cookies.set('showLayerCode', true, { expires: 1 })
		$('.layer-mobile-box').fadeOut(300);
	});

	// console.log(Cookies.get('showLayerCode'));
	if (!Cookies.get('showLayerCode')) {
		$('.layer-mobile-box').fadeIn(300);
	}

	$('.layer-mobile-box').fadeIn(300);


	if ($('#mobileForm').length) {
		let isSubmit = true;
		const form = $('#mobileForm');
		const btnSubmit = form.find('.f-submit');
		form.validate({
			submitHandler: function(form) {
				if (isSubmit) {
					isSubmit = false;
					const postURL = $(form).attr('action');
					const postData = $(form).serialize();

					axios.post(postURL, postData)
					.then(function (res) {
						console.log(res);
						if (res.data.code == '200') {
							$('#mobileForm')[0].reset();
							layer.msg(res.data.msg);
						} else {
							layer.msg(res.data.msg);
						}
						isSubmit = true;
					})
					.catch(function (err) {
						console.log(err);
						// layer.msg(err.data.info)
						layer.msg(err.data.msg);
						isSubmit = true;
					});
				}
			},
			rules: {
				name: {
					required: true
				},
				mobile: {
					required: true,
					isMobile: true,
				},
				code: {
					required: true,
				}
			}
		})
		btnSubmit.on('click', function(){
			form.submit();
		});
	}

})();