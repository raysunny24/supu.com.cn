import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';



export default function() {
	
	const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, .1, 1000);
	camera.position.set(3, 4, 2);
	camera.lookAt(0, 0, 0);

	const scene = new THREE.Scene();
	scene.add(new THREE.AmbientLight(0xffffff));
	scene.backgroundBlurriness = 0.1;
    scene.backgroundIntensity = 0.05;

	const renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.querySelector('.webgl').appendChild(renderer.domElement);

	const loader = new GLTFLoader();
	loader.load('../upload/3d/mcLaren-car-model.glb', function(gltf){
		let model = gltf.scene;
		
		model.traverse((child) => {
			if (child.isMesh) {
				// console.log(child.userData.name);
				if (child.userData.name.includes('Rim')) {
					// console.log(child.userData.name);
					child.material.color.set('#5e5e5e');
				}
				if (child.userData.name.includes('Carpaint') && !child.userData.name.includes('Black') && !child.userData.name.includes('Wiper')) {
					// console.log(child.userData.name);
					child.material.color.set('#ff0000');
				}
			}
		});
		scene.add(model);
	});

	const rgbeLoader = new RGBELoader();
	rgbeLoader.load('../upload/3d/parking_garage_2k.hdr', function(texture) {
		texture.mapping = THREE.EquirectangularReflectionMapping;
		scene.background = texture;
		scene.environment = texture;
	});

	const orbitControl = new OrbitControls(camera, renderer.domElement);

	function animate() {
		requestAnimationFrame(animate);
		renderer.render(scene, camera);
		// ball.rotation.x += .01;
	}
	animate();

}