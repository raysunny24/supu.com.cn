(()=> {
	let CROPPER = null;
	let alt = '';
	let fileType = '';
	let container = '';
	const URL = window.URL || window.webkitURL;
	$(document).on('change', '.fgi-file', function(event){
		const files = event.target.files;
		const container = $(this).parents('.fgi-container');
        if (files && files.length > 0) {

            const file = files[0];

        
			fileType = file.type;
            
            const imgURL = URL.createObjectURL(file);

			alt = file.name.substring(0, file.name.lastIndexOf('.'));

			const temp = `<div class="fgi-preview">
								<h3><span class="icon wb-close js-fgi-delete"></span></h3>
								<img class="fgi-preview__img" src="${imgURL}" alt="">
								<div class="fgi-name">${alt}</div>
							</div>`;
			container.find('.fgi-body').html(temp);
        }

	});
	$(document).on('click', '.js-fgi-delete', function(e){
		e.stopPropagation();
		const container = $(this).parents('.fgi-container');
		const fileInput = container.find('.fgi-file');
		fileInput.val('');
		const id = fileInput.attr('id');
		const temp = `<div class="fgi-select-img">
						<label class="fgi-label" for="${id}">
							<i class="icon wb-plus"></i>
							<span>选择图片</span>
						</label>
					</div>`;
		container.find('.fgi-body').html(temp);
	});

	$(document).on('click', '.fgi-preview', function(e){
		e.stopPropagation();
		const _this = $(this);
		container = $(this).parents('.fgi-container');
		const imgurl = $(this).find('img').attr('src');
		const alt = $(this).find('.fgi-name').html();
		const img = new Image();
		img.src = imgurl;
		img.onload = function(){
			const temp = `<div class="layer-fgi">
							<div class="lfgi-body">
								<span class="icon wb-close lfgi-close js-lfgi-close"></span>
								<div class="lfgi-alt">
									<label class="lfgi-alt__label">Title</label>
									<input class="lfgi-alt__input" type="text" value="${alt}">
								</div>
								<div class="lfgi-cropper">
									<img id="lfgiImage" src="${imgurl}" alt="">
								</div>
								<div class="lfgi-handle">
									<button class="lfgi-btn js-lfgi-btn wb-refresh" data-handle="rotateleft" title="Rotate Left"></button>
									<button class="lfgi-btn js-lfgi-btn wb-reload" data-handle="rotateright" title="Rotate Right"></button>
									<button class="lfgi-btn js-lfgi-btn wb-zoom-in" data-handle="zoomin" title="Zoom In"></button>
									<button class="lfgi-btn js-lfgi-btn wb-zoom-out" data-handle="zoomout" title="Zoom Out"></button>
									<button class="lfgi-btn js-lfgi-btn wb-move" data-handle="move" title="Move"></button>
									<button class="lfgi-btn js-lfgi-btn wb-crop" data-handle="crop" title="Crop"></button>
									<button class="lfgi-btn js-lfgi-btn wb-loop" data-handle="reset" title="Reset"></button>
									<button class="lfgi-btn js-lfgi-btn" data-handle="done">完成裁剪</button>
								</div>
								<div class="lfgi-loading">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
										<path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
											<animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite" />
										</path>
									</svg>
								</div>
							</div>
						</div>`;
			$('body').append(temp);
			const image = document.getElementById('lfgiImage');
			CROPPER = new Cropper(image, {
				// aspectRatio: 640 / 456,
				autoCrop: true,
				autoCropArea: 1,
				minContainerHeight: 400,
				// preview: [document.querySelector('.fgi-editor')]
			});
		}
	});

	$(document).on('click', '.js-lfgi-btn', function(){
		const handle = $(this).data('handle');
		// console.log(handle);
		const fileInputId = container.find('.fgi-file').attr('id');
		switch (handle) {
			case 'rotateleft':
				CROPPER.rotate(-45);
				break;
			case 'rotateright':
				CROPPER.rotate(45);
				break;
			case 'zoomin':
				CROPPER.zoom(0.1);
				break;
			case 'zoomout':
				CROPPER.zoom(-0.1);
				break;
			case 'move':
				console.log(handle);
				CROPPER.setDragMode("move");
				break;
			case 'crop':
				console.log(handle);
				CROPPER.setDragMode("crop");
				break;
			case 'reset':
				CROPPER.reset();
				break;
			case 'done':

				CROPPER.getCroppedCanvas({
					// minWidth: 320,
					// minHeight: 180,
					// maxWidth: 1000,
					// maxHeight: 1000,
					// fillColor: '#fff',
					imageSmoothingEnabled: true,
					imageSmoothingQuality: 'high',
				}).toBlob((blob) => {
					
					// Pass the image file name as the third parameter if necessary.

					const myFile = new File([blob], Date.now()+'.'+blob.type.substring(6, blob.type.length), {
						type: blob.type,
					});
	
					const url = URL.createObjectURL(myFile);
					container.find('.fgi-preview__img').attr('src', url);

				}, fileType);

				break;
			default:
				break;
		}
	});
	
	$(document).on('click', '.js-lfgi-close', function(e){
		$('.layer-fgi').fadeOut(300, function(){
			CROPPER.destroy();
			$('.layer-fgi').remove();
		});
	});

	let isComposition = true;
	$(document).on('input', '.lfgi-alt__input', function(e){
		if (isComposition) {
			console.log(e.target.value);
			container.find('.fgi-name').html(e.target.value);
		}
	});
	$(document).on('compositionstart', '.lfgi-alt__input', function(e){
		isComposition = false;
	});
	$(document).on('compositionend', '.lfgi-alt__input', function(e){
		isComposition = true;
		console.log(e.target.value);
		container.find('.fgi-name').html(e.target.value);
	});

})();