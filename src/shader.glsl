
void mainImage(out vec4 fragColor, in vec2 fragCoord) {
	
	vec2 uv = (fragCoord.xy / iResolution.xy - .5) * 2.0;

	uv = fract(uv * 1.);

	uv.x *= iResolution.x / iResolution.y;

	float len = length(uv);


	vec3 color = vec3(uv.x, uv.y, 0.0);
	
	fragColor = vec4(color, 1.0);

}