const {src, dest, parallel, series} = require('gulp');
const browserSync = require('browser-sync');
const ssi = require('browsersync-ssi');
const sass = require('gulp-sass')(require('sass'));
const watch = require('gulp-watch');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const sftp = require('gulp-sftp-up4');
const os = require('os');


let browser = '';
switch(os.platform()) {
    case 'win32':
        browser = 'chrome';
        break;
    case 'darwin': 
        browser = 'google chrome';
        break;
    default:
}

// 本地环境 START---------------------------------------------------
// 多端同步浏览 (windowns环境下 browser 设置为 chrome)
function server(cb) {
    
	browserSync.init({
        files:[
            './dist/**/*.shtml',
            './dist/**/*.html',
            './dist/scripts/*.js',
            './dist/**/*.css'
        ],
        logLevel: "debug",
        logPrefix: "insgeek",
        // proxy: "http://localhost/web/dist",
		server: {
			baseDir: './dist',
			directory: false,
            middleware: ssi({
                baseDir: __dirname + '/dist',
                ext: '.shtml'
            })
		},
		port: 8080,
        ghostMode: {
            clicks: false,
            froms: false,
            scroll: false
        },
        open: true,
        browser: browser
    });
	cb();
}

// 编译SASS
function scss(cb) {
	return src('./dist/sass/**/*.scss')
        .pipe(sourcemaps.init())
		.pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(sourcemaps.write('.'))
		.pipe(dest('./dist/dflip/styles'))
	cb();
}

// 监听文件更改
function watchScss(cb) {
	const watcher = watch('./dist/**/*.scss', parallel('scss'));
    watcher.on('all', function(event, path, stats){
        console.log('File ' + path + ' was ' + event);
    });
    cb();
}

exports.server = server;
exports.scss = scss;
exports.watchScss = watchScss;
exports.dev = parallel(server, watchScss);