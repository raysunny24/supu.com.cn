/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./dist/**/*.shtml", "./dist/**/*.html"],
    theme: {
        extend: {},
        screens: {
            'sm': {'max': '640px'},
            'md': {'max': '768px'},
            'lg': {'max': '1024px'},
            'xl': {'max': '1280px'},
            'hd': {'max': '1920px'}
        },
        maxWidth: {
            '1280': '1280px'
        },
        colors: {
            'white': '#fff',
            'black': '#000',
            'black80': '#333',
            'black60': '#666',
            'red': '#f00',
            'blue': '#4169e2',
            'lightgray': '#F7F7F7',
            'none': 'rgba(255,255,255,0)',
            'green': '#00A539'
        },
        fontSize: {
            '12': '12px',
            '14': '14px',
            '16': '16px',
            '17': '17px',
            '18': '18px',
            '20': '20px',
            '24': '24px',
            '36': '36px',
            '48': '48px',
            '60': '60px',
            '72': '72px',
        },
        spacing: {
            '0': '0',
            '5': '5px',
            '8': '8px',
            '10': '10px',
            '12': '12px',
            '15': '15px',
            '18': '18px',
            '20': '20px',
            '24': '24px',
            '30': '30px',
            '40': '40px',
            '44': '44px',
            '50': '50px',
            '56': '56px',
            '60': '60px',
            '70': '70px',
            '80': '80px',
            '100': '100px',
            '120': '120px',
            '140': '140px',
            '160': '160px',
            '200': '200px',
            '100vh': '100vh'
        },
        borderRadius: {
            '10': '10px',
            '20': '20px',
            '30': '30px',
            '50': '50px',
            'full': '50%'
        },
        aspectRatio: {
            '10/1': '10 / 1'
        },
        zIndex: {
            '1': '1',
            '10': '10',
            '50': '50',
            '100': '100'
        },
        
        textIndent: {
            '2': '2em'
        },
        fontFamily: {
            sans: ['Source Han Sans CN', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Helvetica', 'Arial', 'Sans-Serif'],
            serif: ['Source Han Serif CN', 'Source Han Serif SC', 'STSong','SimSun','MingLiU','宋体', 'serif'],
            en: ['Maitree']
        },
        fontWeight: {
            '100': '100',
            '200': '200',
            '300': '300',
            '400': '400',
            '500': '500',
            '600': '600',
            '700': '700',
            '800': '800',
            '900': '900',
        },
        letterSpacing: {
            '30': '.03em',
            '50': '.05em',
            '100': '.1em'
        },
        lineHeight: {
            '1': '1',
            '125': '1.25',
            '15': '1.5',
            '2': '2'
        }
    },
    plugins: [
        // require('@tailwindcss/typography')
    ],
}

